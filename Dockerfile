FROM node:17-bullseye-slim

USER root
# Python 3 and make are required to build node-re2
RUN apt-get update && apt-get install -y python3-minimal build-essential git wget bash
# force python3 for node-gyp
RUN rm -rf /usr/bin/python && ln /usr/bin/python3 /usr/bin/python

RUN npm install -g npm@8.1.0

RUN npm install -g renovate@32.10.2 && node --version && npm --version